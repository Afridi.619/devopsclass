Feature: CucumberJava

Scenario Outline: Get Api Test
Given I have Get Api "<Api>" "<City Name>" 
When I hit the api status code is "<Status Code>"
Then status line is "<Status Line>" 


Examples:
|Api											  |City Name||Status Code |Status Line|
|http://restapi.demoqa.com/utilities/weather/city |/New Delhi    ||200		   |HTTP/1.1 200 OK|


