package stepDefinition;

import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class StepDefinition {
	Response res;
	
	@Given("^I have Get Api \"([^\"]*)\" \"(.*)\"$")
	public void i_have_Get_Api(String Api, String cityName) throws Throwable {
		RestAssured .baseURI=Api;
		RequestSpecification httpReq=RestAssured.given();
		 res=httpReq.request(Method.GET,cityName);   
	}

	@When("^I hit the api status code is \"([^\"]*)\"$")
	public void i_hit_the_api_status_code_is(int sCode) throws Throwable {
		String resBody=res.getBody().asString();

		System.out.println("response "+resBody);	
		int statusCode=res.getStatusCode();
		System.out.println("Status Code: "+statusCode);
		Assert.assertEquals(sCode, statusCode);	   
	}

	@Then("^status line is \"([^\"]*)\"$")
	public void status_line_is(String sLine) throws Throwable {
		String statusLine=res.statusLine();
		System.out.println("Status Line: "+statusLine);
		Assert.assertEquals(sLine, statusLine);
	   
	}

}
