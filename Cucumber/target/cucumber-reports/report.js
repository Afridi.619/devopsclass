$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/dataFeature.feature");
formatter.feature({
  "line": 1,
  "name": "CucumberJava",
  "description": "",
  "id": "cucumberjava",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Get Api Test",
  "description": "",
  "id": "cucumberjava;get-api-test",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I have Get Api \"\u003cApi\u003e\" \"\u003cCity Name\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I hit the api status code is \"\u003cStatus Code\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "status line is \"\u003cStatus Line\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "line": 9,
  "name": "",
  "description": "",
  "id": "cucumberjava;get-api-test;",
  "rows": [
    {
      "cells": [
        "Api",
        "City Name",
        "",
        "Status Code",
        "Status Line"
      ],
      "line": 10,
      "id": "cucumberjava;get-api-test;;1"
    },
    {
      "cells": [
        "http://restapi.demoqa.com/utilities/weather/city",
        "/Jodhpur",
        "",
        "200",
        "HTTP/1.1 200 OK"
      ],
      "line": 11,
      "id": "cucumberjava;get-api-test;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 11,
  "name": "Get Api Test",
  "description": "",
  "id": "cucumberjava;get-api-test;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I have Get Api \"http://restapi.demoqa.com/utilities/weather/city\" \"/Jodhpur\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I hit the api status code is \"200\"",
  "matchedColumns": [
    3
  ],
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "status line is \"HTTP/1.1 200 OK\"",
  "matchedColumns": [
    4
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "http://restapi.demoqa.com/utilities/weather/city",
      "offset": 16
    },
    {
      "val": "/Jodhpur",
      "offset": 67
    }
  ],
  "location": "StepDefinition.i_have_Get_Api(String,String)"
});
formatter.result({
  "duration": 7738035100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 30
    }
  ],
  "location": "StepDefinition.i_hit_the_api_status_code_is(int)"
});
formatter.result({
  "duration": 74685500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "HTTP/1.1 200 OK",
      "offset": 16
    }
  ],
  "location": "StepDefinition.status_line_is(String)"
});
formatter.result({
  "duration": 134100,
  "status": "passed"
});
});