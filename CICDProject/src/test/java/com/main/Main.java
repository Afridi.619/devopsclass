package com.main;

import java.io.File;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class Main {

int no=10;
ExtentReports extent;
ExtentTest logger;   
@BeforeTest
public void startReport(){
extent = new ExtentReports (System.getProperty("user.dir") +"/test-output/ExtentReport.html", true);
extent.addSystemInfo("Host Name", "SoftwareTestingMaterial")
.addSystemInfo("Environment", "Automation Testing")
.addSystemInfo("User Name", "Afridi");
extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));

}               
@Test
public void passTest(){
logger = extent.startTest("passTest");
Assert.assertTrue(true);
logger.log(LogStatus.PASS, "Test Case Passed is passTest");
}

@Test
public void failTest(){
logger = extent.startTest("failTest");
Assert.assertTrue(true);
logger.log(LogStatus.PASS, "Test Case (failTest) Status is passed");
}

@Test
public void skipTest() throws Exception{
logger = extent.startTest("skipTest");
}

@AfterMethod
public void getResult(ITestResult result){
if(no == 10){
logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getName());
logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getThrowable());
}else {
logger.log(LogStatus.SKIP, "Test Case Skipped is "+result.getName());
}
extent.endTest(logger);
}
@AfterTest
public void endReport(){     
extent.flush();
extent.close();

}

}
